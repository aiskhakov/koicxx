#ifndef KOICXX_ERASE_REMOVE_HPP
#define KOICXX_ERASE_REMOVE_HPP

#include <algorithm>
#include <iterator>

namespace koicxx {

template <typename CONTAINER_TYPE, typename ELEMENT_TYPE>
void erase_remove(CONTAINER_TYPE& container, const ELEMENT_TYPE& element)
{
  container.erase(
    std::remove(
      std::begin(container)
      , std::end(container)
      , element)
    , std::end(container));
}

} // namespace koicxx

#endif // !KOICXX_ERASE_REMOVE_HPP
