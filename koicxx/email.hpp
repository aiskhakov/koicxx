﻿#ifndef KOICXX_EMAIL_HPP
#define KOICXX_EMAIL_HPP

# ifdef _MSC_VER
#  define NOMINMAX
# endif // _MSC_VER

#include <koicxx/encoding.hpp>
#include <koicxx/exception.hpp>
#include <koicxx/filesystem.hpp>
#include <koicxx/make_string.hpp>
#include <koicxx/random.hpp>

#include <boost/asio.hpp>

#ifdef KOICXX_EMAIL_INCLUDE_SSL

#  ifdef _MSC_VER

#    if defined(DEBUG) || defined(_DEBUG)

#      pragma comment(lib, "libeay32MTd.lib")
#      pragma comment(lib, "ssleay32MTd.lib")

#    else

#      pragma comment(lib, "libeay32MT.lib")
#      pragma comment(lib, "ssleay32MT.lib")

#    endif

#  else

#    pragma comment(lib, "libeay32.lib")
#    pragma comment(lib, "ssleay32.lib")

#  endif

#endif // KOICXX_EMAIL_INCLUDE_SSL

#ifdef KOICXX_EMAIL_INCLUDE_SSL
#  include <boost/asio/ssl.hpp>
#endif

#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include <iterator>
#include <string>
#include <vector>

namespace koicxx {
namespace email {

class exception : public koicxx::exception {};

namespace detail {

#ifdef KOICXX_EMAIL_INCLUDE_SSL
typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
#endif

const int SERVICE_READY     = 220;
const int MAIL_ACTION_OK    = 250;
const int AUTH_SUCCESS      = 235;
const int AUTH_IN_PROCESS   = 334;
const int START_MAIL_INPUT  = 354;

const std::string ALLOWED_CHARACTERS_FOR_BOUNDARY =
  "abcdefghijklmnopqrstuvwxyz"
  "0123456789";
const int BOUNDARY_LENGTH = 50;

template <typename SOCKET_TYPE>
void send_command(SOCKET_TYPE& socket, const std::string& data)
{
  boost::asio::streambuf request;
  std::ostream request_stream(&request);
  request_stream << data << "\r\n";
  boost::asio::write(socket, request);
}

struct smtp_server_response
{
  smtp_server_response() : ret_code(-1) {}

  int ret_code;
  std::string last_line_text;
};

template <typename SOCKET_TYPE>
smtp_server_response get_server_response(SOCKET_TYPE& socket)
{
  smtp_server_response res;

  while (true)
  {
    /**
     * Trying to get first chunk of the server response.
     * According to the boost documentation (http://www.boost.org/doc/libs/1_55_0/doc/html/boost_asio/reference/read_until/overload3.html),
     * "After a successful read_until operation, the streambuf may contain additional data beyond the delimiter"
     */
    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");
    const std::string response_str = koicxx::make_string() << &response;

    /**
     * Trying to get last response line
     * According to the SMTP RFC 5321 (https://tools.ietf.org/html/rfc5321),
     * "The format for multiline replies requires that every line, except the
     * last, begin with the reply code, followed immediately by a hyphen,
     * "-" (also known as minus), followed by text.  The last line will
     * begin with the reply code, followed immediately by <SP>, optionally
     * some text, and <CRLF>.
     * [...]
     * In a multiline reply, the reply code on each of the lines MUST be the
     * same"
     */
    boost::smatch results;
    if (boost::regex_search(response_str.begin(), response_str.end(), results, boost::regex("^(\\d+) (.*)$")))
    {
      res.ret_code = std::stoi(results[1]);
      res.last_line_text = results[2];
    }
    else
    {
      // There's another chunk of the server response, so let's wait for it
      continue;
    }

    break;
  }

  return res;
}

inline
std::string get_current_date_time(const char* fmt)
{
  const boost::local_time::time_zone_ptr utc_time_zone(new boost::local_time::posix_time_zone("GMT"));
  boost::local_time::local_time_facet* facet = new boost::local_time::local_time_facet(fmt);

  const boost::posix_time::ptime& my_ptime = boost::posix_time::second_clock::universal_time();
  boost::local_time::local_date_time now(my_ptime, utc_time_zone);

  std::ostringstream date_osstr;
  date_osstr.imbue(std::locale(date_osstr.getloc(), facet));
  date_osstr << now;

  return date_osstr.str();
}

inline
std::string generate_random_string(const std::string& allowed_characters, const int len)
{
  std::string res;
  res.resize(len);

  koicxx::random::random_number_generator<> rng;

  for (int i = 0; i < len; ++i)
  {
    res[i] = allowed_characters[rng.get_random_number<int>(0, allowed_characters.size() - 1)];
  }

  return res;
}

template <typename SOCKET_TYPE>
void wait_for_service_ready(SOCKET_TYPE& socket)
{
  /**
   * According to the SMTP RFC 5321 (https://tools.ietf.org/html/rfc5321),
   * "Normally, a receiver will send a 220 "Service ready" reply when the connection is
   * completed. The sender SHOULD wait for this greeting message before
   * sending any commands"
   */
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != SERVICE_READY)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while waiting for \"Service Ready\" message from the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_ehlo(SOCKET_TYPE& socket, const std::string& smtp_server_addr)
{
  send_command(socket, "EHLO " + smtp_server_addr);
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"EHLO\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void auth_login(SOCKET_TYPE& socket, const std::string& email, const std::string& password)
{
  send_command(socket, "AUTH LOGIN");
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != AUTH_IN_PROCESS)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"AUTH LOGIN\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }

  send_command(socket, koicxx::encoding::encode_base64(email));
  server_response = get_server_response(socket);
  if (server_response.ret_code != AUTH_IN_PROCESS)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending message contains username to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }

  send_command(socket, koicxx::encoding::encode_base64(password));
  server_response = get_server_response(socket);
  if (server_response.ret_code != AUTH_SUCCESS)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending message contains password to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_mail_from(SOCKET_TYPE& socket, const std::string& from_email)
{
  send_command(socket, "MAIL FROM:<" + from_email + ">");
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"MAIL\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_rcpt_to(SOCKET_TYPE& socket, const std::string& to_email)
{
  send_command(socket, "RCPT TO:<" + to_email + ">");
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"RCPT\" command to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

template <typename SOCKET_TYPE>
void send_data(
  SOCKET_TYPE& socket
  , const std::string& from_email
  , const std::string& to_email
  , const std::string& subject
  , const std::string& body
  , const std::vector<boost::filesystem::path> attachments
)
{
  send_command(socket, "DATA");
  /**
   * According to the SMTP RFC 5321 (https://tools.ietf.org/html/rfc5321),
   * "message data MUST NOT be sent unless a 354 reply is received"
   */
  smtp_server_response server_response = get_server_response(socket);
  if (server_response.ret_code != START_MAIL_INPUT)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending \"DATA\" message to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }

  send_command(socket, "From: <" + from_email + ">");
  send_command(socket, "To: " + to_email);
  send_command(socket, "Subject: " + subject);
  send_command(socket, "Date: " + get_current_date_time("%d %b %Y %T %q")); // Example: "29 Apr 2014 06:47:38 +0000"
  if (attachments.empty())
  {
    send_command(socket, "");
    send_command(socket, body);
  }
  else
  {
    std::string boundary;
    while (true)
    {
      boundary = generate_random_string(
        ALLOWED_CHARACTERS_FOR_BOUNDARY
        , BOUNDARY_LENGTH);

      // Check whether boundary contains in the data
      std::string email_content(body);
      for (const boost::filesystem::path& attachment : attachments)
      {
        email_content += koicxx::encoding::encode_base64(attachment);
      }

      if (email_content.find(boundary) == std::string::npos)
      {
        break;
      }
    }

    send_command(socket, "MIME-Version: 1.0");
    send_command(socket, "Content-Type: multipart/mixed; boundary=" + boundary);
    send_command(socket, "");
    send_command(socket,
      "This is a MIME formatted message. If you see this text it means that your"
      " email software does not support MIME formatted messages");
    send_command(socket, "");
    send_command(socket, "--" + boundary);
    send_command(socket, "Content-Type: text/plain;");
    send_command(socket, "");
    send_command(socket, body);

    for (const boost::filesystem::path& attachment : attachments)
    {
      const std::string& attachment_mime_type = koicxx::filesystem::get_mime_type(attachment);
      const std::string& attachment_filename = attachment.filename().string();

      send_command(socket, "");
      send_command(socket, "--" + boundary);
      send_command(socket, "Content-Type: " + attachment_mime_type + "; name=\"" + attachment_filename + "\"");
      send_command(socket, "Content-Disposition: attachment; filename=\"" + attachment_filename + "\"");
      send_command(socket, "Content-Transfer-Encoding: base64");
      send_command(socket, "");
      send_command(socket, koicxx::encoding::encode_base64(attachment));
    }

    send_command(socket, "--" + boundary + "--");
  }

  send_command(socket, ".");

  server_response = get_server_response(socket);
  if (server_response.ret_code != MAIL_ACTION_OK)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        koicxx::make_string()
          << "An error occurred while sending message contains data to the server."
          << " Result: " << server_response.ret_code << ", last line text: " << server_response.last_line_text
    );
  }
}

} // namespace detail

enum class crypt_protocol { NONE, SSL };

inline
void send_email(
  const std::string& smtp_server_addr
  , const int smtp_server_port
  , const std::string& from
  , const std::string& password
  , const std::string& to
  , const std::string& subject
  , const std::string& body
  , const crypt_protocol crypt_proto = crypt_protocol::NONE
  , const std::vector<boost::filesystem::path> attachments = std::vector<boost::filesystem::path>()
)
{
  try
  {
    if (crypt_proto == crypt_protocol::NONE)
    {
      boost::asio::io_service io_service;
      boost::asio::ip::tcp::resolver resolver(io_service);
      boost::asio::ip::tcp::resolver::query query(smtp_server_addr, boost::lexical_cast<std::string>(smtp_server_port));

      boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
      boost::asio::ip::tcp::socket socket(io_service);
      boost::asio::connect(socket, endpoint_iterator);

      detail::wait_for_service_ready(socket);
      detail::send_ehlo(socket, smtp_server_addr);
      detail::auth_login(socket, from, password);
      detail::send_mail_from(socket, from);
      detail::send_rcpt_to(socket, to);
      detail::send_data(socket, from, to, subject, body, attachments);
    }
#ifdef KOICXX_EMAIL_INCLUDE_SSL
    else if (crypt_proto == crypt_protocol::SSL)
    {
      boost::asio::ssl::context ctx(boost::asio::ssl::context::sslv23);
      ctx.set_default_verify_paths();

      boost::asio::io_service io_service;
      detail::ssl_socket socket(io_service, ctx);
      boost::asio::ip::tcp::resolver resolver(io_service);
      boost::asio::ip::tcp::resolver::query query(smtp_server_addr, boost::lexical_cast<std::string>(smtp_server_port));

      boost::asio::connect(socket.lowest_layer(), resolver.resolve(query));
      socket.lowest_layer().set_option(boost::asio::ip::tcp::no_delay(true));

      socket.set_verify_mode(boost::asio::ssl::context::verify_none);
      socket.handshake(detail::ssl_socket::client);

      detail::wait_for_service_ready(socket);
      detail::send_ehlo(socket, smtp_server_addr);
      detail::auth_login(socket, from, password);
      detail::send_mail_from(socket, from);
      detail::send_rcpt_to(socket, to);
      detail::send_data(socket, from, to, subject, body, attachments);
    }
#endif //KOICXX_EMAIL_INCLUDE_SSL
    else
    {
      KOICXX_THROW(exception())
        << koicxx::exception::reason(
          "Unsupported crypt. protocol"
      );
    }
  }
  catch (const exception& ex)
  {
    KOICXX_THROW(exception())
      << koicxx::exception::reason(
        ex.reason()
    );
  }
  catch (const std::exception& ex)
  {
  	KOICXX_THROW(exception())
      << koicxx::exception::reason(
        ex.what()
    );
  }
}

} // namespace email
} // namespace koicxx

#endif // !KOICXX_EMAIL_HPP
