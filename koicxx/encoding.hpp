#ifndef KOICXX_ENCODING_HPP
#define KOICXX_ENCODING_HPP

#include <koicxx/filesystem.hpp>

#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>

# ifdef _MSC_VER
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
# endif // _MSC_VER

#include <cstdint>
#include <cstdio>
#include <cwctype>
# ifdef _MSC_VER
#  include <codecvt>
# endif // _MSC_VER
#include <locale>
#include <sstream>
#include <string>

namespace koicxx {
namespace encoding {

#ifdef _MSC_VER

/**
 * \brief Convert a std::wstring object to an UTF8 std::string object
 * \return UTF8-encoded std::string object on success, empty string otherwise
 *
 * \todo Add non-MSVC version
 */
inline
std::string utf8_encode(const std::wstring& wstr)
{
  const int size_needed = WideCharToMultiByte(
    CP_UTF8
    , 0
    , &wstr[0]
    , static_cast<int>(wstr.size())
    , NULL
    , 0
    , NULL
    , NULL
  );
  if (!size_needed)
  {
    return std::string();
  }
  std::string out(size_needed, 0);
  const int res = WideCharToMultiByte(
    CP_UTF8
    , 0
    , &wstr[0]
    , static_cast<int>(wstr.size())
    , &out[0]
    , size_needed
    , NULL
    , NULL
  );
  if (!res)
  {
    return std::string();
  }
  return out;
}

/**
 * \brief Convert an UTF8 std::string object to a std::wstring object
 * \return std::wstring object on success, empty string otherwise
 *
 * \todo Add non-MSVC version
 */
inline
std::wstring utf8_decode(const std::string& str)
{
  const int size_needed = MultiByteToWideChar(
    CP_UTF8
    , 0
    , &str[0]
    , static_cast<int>(str.size())
    , NULL
    , 0
  );
  if (!size_needed)
  {
    return std::wstring();
  }
  std::wstring out(size_needed, 0);
  const int res = MultiByteToWideChar(
    CP_UTF8
    , 0
    , &str[0]
    , static_cast<int>(str.size())
    , &out[0]
    , size_needed
  );
  if (!res)
  {
    return std::wstring();
  }
  return out;
}

/**
 * \brief Convert an UTF8 std::string object to a UTF16 std::string object
 */
inline
std::string convert_utf8_string_to_utf16_string(const std::string& utf8_string)
{
  std::wstring_convert<std::codecvt_utf16<wchar_t>> myconv;
  std::wstring utf8_decoded_string = utf8_decode(utf8_string);
  std::string bs = myconv.to_bytes(utf8_decoded_string);
  return bs;
}

#endif // _MSC_VER

/**
 * \brief This function does the URL encoding for the specified std::string object
 */
inline
std::string url_encode(const std::string& input)
{
  const std::string hex_symbols("0123456789ABCDEF");

  // RFC 3986 section 2.3 Unreserved Characters (January 2005)
  const std::string unreserved_characters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~");

  std::string res;

  for (const auto cur_char : input)
  {
    if (unreserved_characters.find(cur_char) != std::string::npos)
    {
      res += cur_char;
    }
    else
    {
      res += '%';
      res += hex_symbols[(unsigned char)cur_char / 16];
      res += hex_symbols[(unsigned char)cur_char % 16];
    }
  }

  return res;
}

#ifdef _MSC_VER

/**
 * \brief This function does the URL encoding for the specified std::string object
 */
inline
std::string url_encode(const std::wstring& input)
{
  std::string utf8_str = utf8_encode(input);
  return url_encode(utf8_str);
}

#endif // _MSC_VER

/**
 * \brief This function does the HTML encoding for the specified std::wstring object
 */
inline
std::string html_encode(const std::wstring& input)
{
  std::ostringstream result;
  for (auto itr = input.begin(), end = input.end(); itr != end; ++itr)
  {
    if (*itr >= 128)
    {
      result << "&#" << static_cast<int>(*itr) << ";";
    }
    else if (*itr == '<')
    {
      result << "&lt;";
    }
    else if (*itr == '>')
    {
      result << "&gt;";
    }
    else if (*itr == '&')
    {
      result << "&amp;";
    }
    else if (*itr == '\"')
    {
      result << "&quot;";
    }
    else
    {
      result << static_cast<char>(*itr); // Copy untranslated
    }
  }
  return result.str();
}

typedef boost::archive::iterators::base64_from_binary<boost::archive::iterators::transform_width<const char *, 6, 8>> base64_text;

inline
std::string encode_base64(const std::string& data)
{
  std::stringstream os;
  const std::size_t sz = data.size();
  const char* c_str_data = data.c_str();
  std::copy(base64_text(c_str_data), base64_text(c_str_data + sz), std::ostream_iterator<char>(os));
  const std::string base64_padding[] = { "", "==", "=" };
  os << base64_padding[sz % 3];
  return os.str();
}

inline
std::string encode_base64(const boost::filesystem::path& filepath)
{
  const std::string& data = koicxx::filesystem::get_file_content(filepath, koicxx::filesystem::file_type::BINARY);
  return encode_base64(data);
}

} // namespace encoding
} // namespace koicxx

#endif // !KOICXX_ENCODING_HPP