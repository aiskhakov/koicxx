#ifndef KOICXX_FIBER_HPP
#define KOICXX_FIBER_HPP

# ifndef _MSC_VER
#  error This is MSVC-specific header file
# endif // !_MSC_VER

#include <koicxx/exception.hpp>
#include <koicxx/make_string.hpp>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <cstddef>

namespace koicxx {

  class fiber_exception : public koicxx::exception {};

  /**
   * \brief Simple implementation of fibers
   */
  class fiber
  {
  public:
    /**
     * \throw koicxx::fiber_exception on failure
     */
    static fiber convert_current_thread_to_fiber()
    {
      return create_fiber(NULL);
    }

    /**
     * \throw koicxx::fiber_exception on failure
     */
    static fiber create_fiber(LPFIBER_START_ROUTINE start_addr, void* params = NULL, std::size_t stack_size = 0)
    {
      return fiber(start_addr, params, stack_size);
    }

    static void switch_to_fiber(const fiber& fiber)
    {
      SwitchToFiber(fiber.get_addr());
    }

    void delete_fiber()
    {
      DeleteFiber(_addr);
    }

    void* get_addr() const
    {
      return _addr;
    }

  private:
    fiber(LPFIBER_START_ROUTINE start_addr, void* params, std::size_t stack_size)
    {
      if (start_addr == NULL)
      {
        _addr = ConvertThreadToFiber(NULL);
      }
      else
      {
        _addr = CreateFiber(stack_size, start_addr, params);
      }

      if (_addr == NULL)
      {
        DWORD error_code = GetLastError();
        KOICXX_THROW(fiber_exception())
          << koicxx::exception::reason(make_string() << "Unable to create fiber. Error code: " << error_code)
          << koicxx::exception::error_code(error_code);
      }
    }

    void* _addr;
  };

} // namespace koicxx

#endif // !KOICXX_FIBER_HPP