#ifndef KOICXX_MACRO_MAGIC_HPP
#define KOICXX_MACRO_MAGIC_HPP

#include <cstddef>
#include <cstring>

/**
 * \brief Calculates an array size
 */
template <typename T, std::size_t N>
char (&array_size_helper(T (&arr)[N]))[N];
#define KOICXX_ARRAY_SIZE(arr) (sizeof(array_size_helper(arr)))

/**
 * \brief Copy an C-string in a more safe manner
 */
# ifdef _MSC_VER
#  define KOICXX_COPY_STR(dst, src) { strncpy_s(dst, src, sizeof(dst) - 1); } // or _TRUNCATE
# else
#  define KOICXX_COPY_STR(dst, src) { strncpy(dst, src, sizeof(dst) - 1); dst[sizeof(dst) - 1] = 0; }
# endif

/**
 * \brief Disables the specified warning
 */
# ifdef _MSC_VER
#  define KOICXX_DISABLE_WARNING(WARN, CODE)  \
  __pragma(warning(push))                     \
  __pragma(warning(disable:WARN))             \
  CODE                                        \
  __pragma(warning(pop))
# endif

#endif // !KOICXX_MACRO_MAGIC_HPP