#ifndef KOICXX_IO_HPP
#define KOICXX_IO_HPP

#include <koicxx/to_underlying.hpp>

#include <boost/scope_exit.hpp>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <iostream>
#include <string>

namespace koicxx {
namespace io {

enum class color { BLUE = FOREGROUND_BLUE, GREEN = FOREGROUND_GREEN, RED = FOREGROUND_RED, YELLOW = 6, GREY = 8, WHITE = 15 };

bool print(const std::string& text, color text_color)
{
  HANDLE std_output_handle = GetStdHandle(STD_OUTPUT_HANDLE);
  if (std_output_handle == INVALID_HANDLE_VALUE)
  {
    return false;
  }

  CONSOLE_SCREEN_BUFFER_INFO prev_console_screen_buffer_info = { 0 };
  if (GetConsoleScreenBufferInfo(std_output_handle, &prev_console_screen_buffer_info) == 0)
  {
    return false;
  }
  BOOST_SCOPE_EXIT_ALL(std_output_handle, &prev_console_screen_buffer_info)
  {
    SetConsoleTextAttribute(std_output_handle, prev_console_screen_buffer_info.wAttributes);
  };

  if (SetConsoleTextAttribute(std_output_handle, to_underlying(text_color)) == 0)
  {
    return false;
  }

  std::cout << text;
  
  return true;
}

} // namespace io
} // namespace koicxx

#endif // !KOICXX_IO_HPP
