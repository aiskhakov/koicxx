cmake_minimum_required(VERSION 2.6)

project (tests)

set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON)
find_package(Boost COMPONENTS filesystem system thread date_time chrono regex unit_test_framework REQUIRED)
find_package(OpenSSL REQUIRED)
include_directories(${Boost_INCLUDE_DIR} ${OPENSSL_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR} ${OPENSSL_LIBRARIES})

set (SOURCES 
	main.cpp) 
 
add_executable (${PROJECT_NAME} ${SOURCES})
target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES} ${OPENSSL_LIBRARIES})

enable_testing ()
add_test (${PROJECT_NAME} ${PROJECT_NAME})
